@extends("layouts.app")

@section("content")
        <div class="container">
                <h2>Lista de Campañas</h2>
                <a href="{{route("listAll")}}">Ver Campañas Guardadas</a>

                <table class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Type</th>
                            <th scope="col">options</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($campaigns as $campaign)
                                <tr>
                                    <td scope="row">{{$campaign["id"]}}</td>
                                    <td>{{$campaign["name"]}}</td>
                                    <td>{{$campaign["advertiser_ad_type_label"]}}</td>
                                    <td>
                                      <form action="{{route("save")}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="campaignid" value="{{$campaign["id"]}}">
                                        <button type="submit">Guardar</button>
                                      </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
        </div>
@endsection
@extends("layouts.app")

@section("content")
        <div class="container">
                <h2>Lista de Campañas</h2>

                <table class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Type</th>
                            <th scope="col">Zone Targeting Type</th>
                            <th scope="col">options</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($campaigns as $campaign)
                                <tr>
                                    <td scope="row">{{$campaign->id_campaign}}</td>
                                    <td>{{$campaign->name}}</td>
                                    <td>{{$campaign->type}}</td>
                                    <td>{{$campaign->zone_targeting_type}}</td>
                                    <td>
                                        <a href="{{route("campaign_show",["id"=> $campaign->id])}}"> Detalles </a>
                                      
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
@endsection
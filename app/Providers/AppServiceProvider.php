<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Exads\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $baseUrl = env("API_ENDPOINT_EXOCLICK");
        $token = env("API_EXOCLICK_TOKEN");

        $this->app->singleton(Client::class, function($app) use ($baseUrl, $token) {
            $client = new Client($baseUrl);
            return $client->setApiToken($client->login->getToken($token));
    });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

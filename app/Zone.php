<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Campaign;

class Zone extends Model
{
    protected $fillable = [
        "campaign_id", "idzones", "price"
    ];

    public function campaign(){
        
        return $this->belongsTo(Campaign::class);
    }
}

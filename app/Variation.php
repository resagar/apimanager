<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Campaign;

class Variation extends Model
{
    protected $fillable = [
        "campaign_id", "idvariation", "idvariations_file_url", "imageurl_url"
    ];

    public function campaign(){
        
        return $this->belongsTo(Campaign::class);
    }
}

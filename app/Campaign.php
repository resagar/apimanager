<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Zone; use App\Variation;

class Campaign extends Model
{
    protected $table = "campaigns";
    protected $fillable = [
        "id_campaign", "name", "type", "zone_targeting_type"
    ];

    public function variation(){

        return $this->hasMany(Variation::class);

    }

    public function zones(){

        return $this->hasMany(Zone::class);

    }
}
